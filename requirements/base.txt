# Django
# ------------------------------------------------------------------------------
django ~=4.1.5
django-environ ~=0.9.0

django-registration ~=3.3.0

django-countries ~=7.5.0
django-dynamic-filenames ~=1.3.1

django-tapeforms ~=1.1.0
