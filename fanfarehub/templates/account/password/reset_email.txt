{% load i18n %}

{% autoescape off %}
{% blocktrans with name=user.get_short_name trimmed %}
Hi {{ name }},
{% endblocktrans %}

{% blocktrans trimmed %}
To initiate the password reset process for your account on {{ site_name }},
click the link below:
{% endblocktrans %}

	{{ protocol }}://{{ domain }}{% url "account:password_reset_confirm" uidb64=uid token=token %}

{% blocktrans trimmed %}
If clicking the link above doesn't work, please copy and paste the URL in a
new browser window instead.
{% endblocktrans %}
{% endautoescape %}
