from django.conf import settings
from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from fanfarehub.models import Instrument, Profile, Stand, User


class InstrumentInline(admin.StackedInline):
    model = Instrument
    extra = 1


class StandAdmin(admin.ModelAdmin):
    inlines = [InstrumentInline]


class ProfileInline(admin.StackedInline):
    model = Profile
    max_num = 1
    can_delete = False


class UserAdmin(auth_admin.UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_("Identity"), {'fields': ('first_name', 'last_name')}),
        (
            _("Permissions"),
            {
                'fields': (
                    'is_active',
                    'is_superuser',
                    'groups',
                    'user_permissions',
                ),
            },
        ),
        (_("Account details"), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {'fields': ('email', 'password1', 'password2')}),
        (_("Identity"), {'fields': ('first_name', 'last_name')}),
    )
    readonly_fields = ('last_login', 'date_joined')
    inlines = [ProfileInline]
    list_display = ('email', 'first_name', 'last_name', 'is_active')
    list_filter = ('is_superuser', 'is_active', 'groups')
    search_fields = ('first_name', 'last_name', 'email')
    ordering = ('last_name', 'first_name')

    def get_formsets_with_inlines(self, request, obj=None):
        for inline in self.get_inline_instances(request, obj):
            # hide UserProfileInline in the add view
            if not isinstance(inline, ProfileInline) or obj is not None:
                yield inline.get_formset(request, obj), inline


# ADMINISTRATIVE SITE
# ------------------------------------------------------------------------------


class AdminSite(admin.AdminSite):
    site_title = _("FanfareHub Administration")
    site_header = _("FanfareHub Administration")
    index_title = _("Application administration")
    enable_nav_sidebar = False

    def login(self, request, **kwargs):
        index_path = reverse('admin:index', current_app=self.name)
        if request.method == 'GET' and self.has_permission(request):
            return HttpResponseRedirect(index_path)
        return HttpResponseRedirect(
            '{}?next={}'.format(settings.LOGIN_URL, index_path)
        )


admin_site = AdminSite()
admin_site.register(Stand, StandAdmin)
admin_site.register(User, UserAdmin)
# register dependencies in our admin too
admin_site.register(auth_admin.Group, auth_admin.GroupAdmin)
