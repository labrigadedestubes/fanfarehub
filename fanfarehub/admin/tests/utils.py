import pytest

from fanfarehub.tests.utils import ViewMixin


class AdminViewMixin(ViewMixin):
    """
    Extends `ViewMixin` to make requests as an admin user by default.
    """

    @pytest.fixture(autouse=True)
    def setup_user(self, admin_user):
        self.user = admin_user
