from django.conf import settings
from django.urls import include, path

from .admin.admin import admin_site
from .views import account as account_views
from .views.home import HomeView

account_patterns = [
    path('login/', account_views.LoginView.as_view(), name='login'),
    path('logout/', account_views.LogoutView.as_view(), name='logout'),
    path(
        'password/reset/',
        account_views.PasswordResetView.as_view(),
        name='password_reset',
    ),
    path(
        'password/reset/<uidb64>/<token>/',
        account_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm',
    ),
    # Registration's views
    path(
        'activate/<str:activation_key>/',
        account_views.ActivateView.as_view(),
        name='activate',
    ),
    path(
        'register/',
        account_views.RegisterView.as_view(),
        name='register',
    ),
    path(
        'register/closed/',
        account_views.RegistrationClosedView.as_view(),
        name='registration_closed',
    ),
    # Current user's views
    path(
        'password/change/',
        account_views.PasswordChangeView.as_view(),
        name='password_change',
    ),
    path(
        'profile/',
        account_views.ProfileUpdateView.as_view(),
        name='profile_update',
    ),
]

urlpatterns = [
    path('admin/', admin_site.urls),
    path('account/', include((account_patterns, 'account'))),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.views import defaults as default_views

    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            '400/',
            default_views.bad_request,
            kwargs={'exception': Exception('Bad Request!')},
        ),
        path(
            '403/',
            default_views.permission_denied,
            kwargs={'exception': Exception('Permission Denied')},
        ),
        path(
            '404/',
            default_views.page_not_found,
            kwargs={'exception': Exception('Page not Found')},
        ),
        path('500/', default_views.server_error),
    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns.insert(0, path('__debug__/', include(debug_toolbar.urls)))

urlpatterns += [
    path('', HomeView.as_view(), name='home'),
]
